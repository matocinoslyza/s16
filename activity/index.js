/* Create a variable nnumber that will store the value 
of the number provided by the user via the prompt*/
let number = Number(prompt("Enter a number:"));

/* Create a for loop that will be initialized with the
provided by the user, will stop when the value is less than
or equal to 0 and will decreased by 1 every iteration.
*/

for(let i = number; i >=0 ; i--){
	console.log(i);
	
/*Create a condition that if the current value is less than or 
equal to 50, stop the loop
*/
if(number <= 50) {
		break;
	}
}


/*Create another condition that if the current value is 
divisible by 10, print the message that the number is 
being skipped and continue to the next iteration of the loop*/
let number1 = Number(prompt("Enter a number: "));
    for(let i = number1; i >=0 ; i--){
        console.log(i);
    if (i % 10 === 0) {
        console.log("The number is divisible by 10 and is being skipped");
    }
/*create Another condition that if the current value is
divisble by 5, print the number*/
    if (i % 5 === 0) {
        console.log("The number is divisible by 5 which is " + i);
    }
}

/*Create a variable that will contain the string
Supercalifragilisticexpialidocious*/
let myString = "Supercalifragilisticexpialidocious";

/*Create another variable that will store the 
consonants from the string*/
let myConsonants = "Sprclfrglstcxpldcs";

/*Create another loop that will iterate through individual 
letters of string based on its length*/
for(let x = 0; x < myString.length; x++){
    console.log(myString[x]);
}

/*Create an if statement that will check if the
letter of the string is equal to the vowel and continue to
the next iteration of the loop if it is true.
*/
for(let i = 0; i < myString.length; i++){
    if(
        myString[i].toLowerCase() == 'a' ||
        myString[i].toLowerCase() == 'e' ||
        myString[i].toLowerCase() == 'i' ||
        myString[i].toLowerCase() == 'o' ||
        myString[i].toLowerCase() == 'u'
    ){
        continue;
    }
    /*Create an else statement that will add the letter to the 
    second variable*/
    else{
        console.log(myString[i]);
    }
}

